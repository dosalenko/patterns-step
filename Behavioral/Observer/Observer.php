<?php

namespace Behavioral\Observer;

/**
 * we have a model Colli, after it got status 'received'  we need to run a chain
 * of actions for other models, create new record for Booking, send mail to user, save action to database log, etc.
 * Trying to realize without using laravel observer
 */

//The subject class (Colli) that can be observed
class Colli
{
    private $observers = [];
    private $status;

    public function attach($observer)
    {
        $this->observers[] = $observer;
    }

    public function setStatus($status)
    {
        $this->status = $status;
        $this->notify();
    }

    private function notify()
    {
        foreach ($this->observers as $observer) {
            $observer->update($this);
        }
    }

    public function getStatus()
    {
        return $this->status;
    }
}

//Define the observer interface
interface Observer
{
    public function update($colli);
}

//Concrete observers
class BookingObserver implements Observer
{
    public function update($colli)
    {
        if ($colli->getStatus() == 'received') {
            $this->createBooking();
            $this->sendEmail();
            $this->saveToLog();
        }
    }

    private function createBooking()
    {
        // Create a new Booking model
        $booking = new Booking('test', 'Kobenhavn');
        $booking->save();
    }

    private function sendEmail()
    {
        // Send an email to the user//
        echo '--mail send--';

    }

    private function saveToLog()
    {
        // Save the results to a log
        //Log::save('action');
        echo '--log saved--';
    }
}

class Booking
{
    public string $name;
    public string $address;

    /**
     * Booking constructor.
     * @param string $name
     * @param string $address
     */
    public function __construct(string $name, string $address)
    {
        $this->name = $name;
        $this->address = $address;
    }

    public function save()
    {
        //save to db
        echo '--model saved--';
    }
}

$colli = new Colli();
$bookingObserver = new BookingObserver();
$colli->attach($bookingObserver);

// Set the status of the Colli to "received"
$colli->setStatus('received');

