<?php

namespace Behavioral\ChainOfResponsibility;


/**
 * we have a "trip" entity, which contains address, volume, weight, customer id as main parameters.
 * we need to check for available dates for the driver to make this trip and pick up the carriage
 * to his truck.
 * Before check if selected a date is suitable, we have to perform a lot of checking consecutively. The main ones are
 * If the selected date is a weekend or local holiday
 * If the volume and weight of the truck of current day are sufficient
 * If there is a limit to the driver's stops
 */
//The abstract handler class
abstract class Handler
{
    protected $checkFlow;

    public function setCheckFlow(Handler $handler)
    {
        $this->successor = $handler;
    }

    public function handle(Trip $trip)
    {
        if ($this->checkFlow !== null) {
            return $this->checkFlow->handle($trip);
        }
        return true;
    }
}

//Concrete handler
class WeekendHandler extends Handler
{
    public function handle(Trip $trip)
    {
        $pickupDate = $trip->pickupDate;
        $dayOfWeek = date('w', strtotime($pickupDate));
        if ($dayOfWeek == 0 || $dayOfWeek == 6) {
            return false; // weekend day, disallow pickup date
        } else {
            return parent::handle($trip);
        }
    }
}

class HolidayHandler extends Handler
{
    public function handle(Trip $trip)
    {
        $pickupDate = $trip->pickupDate;
        $holidays = ['2023-04-25', '2023-05-01'];
        if (in_array($pickupDate, $holidays)) {
            return false; // local danish  holiday, disallow pickup date
        } else {
            return parent::handle($trip);
        }
    }
}

class CapacityHandler extends Handler
{
    private $maxVolume = 100;
    private $maxWeight = 1000;
    private $currentVolume = 90;
    private $currentWeight = 900;

    public function handle(Trip $trip)
    {
        if ($trip->volume > ($this->maxVolume - $this->currentVolume) || $trip->weight > ($this->maxWeight - $this->currentWeight)) {
            return false; // capacity exceeded, disallow pickup date
        } else {
            return parent::handle($trip);
        }
    }
}

class StopsHandler extends Handler
{
    private $maxStops = 10; // example driver limit
    private $currentStops = 2;

    public function handle(Trip $trip)
    {
        if ($this->currentStops >= $this->maxStops) {
            return false; // stops limit reached, disallow pickup date
        } else {
            return parent::handle($trip);
        }
    }
}

//Trip entity
class Trip
{
    public $address;
    public $volume;
    public $weight;
    public $customerId;
    public $pickupDate;

    /**
     * Trip constructor.
     * @param $address
     * @param $volume
     * @param $weight
     * @param $customerId
     * @param $pickupDate
     */
    public function __construct($address, $volume, $weight, $customerId, $pickupDate)
    {
        $this->address = $address;
        $this->volume = $volume;
        $this->weight = $weight;
        $this->customerId = $customerId;
        $this->pickupDate = $pickupDate;
    }


}

// Set up the chain
$weekendHandler = new WeekendHandler();
$holidayHandler = new HolidayHandler();
$capacityHandler = new CapacityHandler();
$stopsHandler = new StopsHandler();

$weekendHandler->setCheckFlow($holidayHandler);
$holidayHandler->setCheckFlow($capacityHandler);
$capacityHandler->setCheckFlow($stopsHandler);


$trip = new Trip('Kobenhavn K', 90, 1000, 1, '2023-04-25');
$result = $capacityHandler->handle($trip);
if ($result) {
    echo 'Pickup date is allowed';
} else {
    echo 'Pickup date is not allowed';
}




