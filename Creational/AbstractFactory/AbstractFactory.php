<?php

namespace Creational\AbstractFactory;

/**
 * There is need to communicate with client.
 * Customer can check any kind of communication type via sms, email or messenger or all of them.
 * The message should be the same not depends on type
 */
interface Notification
{
    public function send($message);
}

class SmsNotification implements Notification
{
    public function send($message)
    {
        //sms api client connection
        echo strip_tags($message);
        echo "--send via " . __CLASS__;
    }
}

class EmailNotification implements Notification
{
    public function send($message)
    {
        // implementation for sending email message
        echo strip_tags($message);
        echo "--send via " . __CLASS__;
    }
}

interface NotificationFactory
{
    public function createNotification(): Notification;
}

class SmsNotificationFactory implements NotificationFactory
{
    public function createNotification(): Notification
    {
        return new SmsNotification();
    }
}

class EmailNotificationFactory implements NotificationFactory
{
    public function createNotification(): Notification
    {
        return new EmailNotification();
    }
}

class NotificationManager
{
    private $factory;

    public function __construct(NotificationFactory $factory)
    {
        $this->factory = $factory;
    }

    public function sendNotification($message)
    {
        $notification = $this->factory->createNotification();
        $notification->send($message);
    }
}

$message = 'send me';

// Send SMS message
$smsNotificationFactory = new SmsNotificationFactory();
$notificationManager = new NotificationManager($smsNotificationFactory);
$notificationManager->sendNotification($message);

// Send email message
$emailNotificationFactory = new EmailNotificationFactory();
$notificationManager = new NotificationManager($emailNotificationFactory);
$notificationManager->sendNotification($message);