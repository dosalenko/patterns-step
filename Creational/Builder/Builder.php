<?php

namespace Creational\Builder;

/*
 * Determine the set of tools, peoples, cars that are necessary to organize the delivery
 *  and installation of products to the client with the builder pattern ( not using the 'director' class)
 *
 */

interface TripBuilderInterface
{
    public function selectTruck(float $volume, float $weight): TripBuilderInterface;

    public function addWorkers(int $amount): TripBuilderInterface;

    public function addEquipment(bool $isRequired = false): TripBuilderInterface;

    public function getFinalRequest(): string;
}

class StandardDelivery implements TripBuilderInterface
{
    private $request;

    public function __construct()
    {
        $this->reset();
    }

    private function reset(): void
    {
        $this->request = new \stdClass();
    }

    public function selectTruck(float $volume, float $weight): TripBuilderInterface
    {
        $this->reset();
        $truckList = [
            'model1' => [
                'weight' => 100,
                'volume' => 1000,
            ],
            'model2' => [
                'weight' => 1000,
                'volume' => 10000,
            ],
            'model3' => [
                'weight' => 2000,
                'volume' => 20000,
            ]
        ];

        $selectedTruck = null;
        foreach ($truckList as $k => $model) {
            if ($weight < $model['weight'] && $volume < $model['volume']) {
                $selectedTruck = $k;
                break;
            }
        }
        if ($selectedTruck) {
            $this->request->truck = 'Selected truck with capacity: ' . implode(',', $truckList['model2']) . '.';
        } else {
            $this->request->truck = 'No appropriate truck was found.';
        }
        $this->request->type = 'Truck selection';

        return $this;
    }

    public function addWorkers(int $amount): TripBuilderInterface
    {
        $this->request->workers = 'added additional workers: ' . $amount . ' people';
        $this->request->type = 'Worker selection';
        return $this;
    }

    public function addEquipment(bool $isRequired = false): TripBuilderInterface
    {
        $this->request->equipment = $isRequired ? 'added additional equipment.' : 'Without special equipment.';
        $this->request->type = 'Equipment selection';
        return $this;
    }

    public function getFinalRequest(): string
    {
        $result = $this->request->truck;

        if (!empty($this->request->workers)) {
            $result .= ' and ' . $this->request->workers;
        }
        if (isset($this->request->equipment)) {
            $result .= ' and ' . $this->request->equipment;
        }

        return $result;
    }
}

function builder(TripBuilderInterface $builder): void
{
    $query = $builder->selectTruck(100, 100)
        ->addWorkers(5)
        ->addEquipment(true)
        ->getFinalRequest();

    echo $query;
}

builder(new StandardDelivery());



