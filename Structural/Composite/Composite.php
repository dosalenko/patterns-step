<?php

namespace Structural\Composite;

/**
 * abstract example
 */

/*
 * allows to work with objects as a single tree structure
 */
abstract class Employee
{
    protected string $name;
    protected float $salary;

    /**
     * Employee constructor.
     * @param float $name
     * @param string $salary
     */
    public function __construct(string $name, float $salary)
    {
        $this->name = $name;
        $this->salary = $salary;
    }

    /**
     * @return string
     */
    abstract public function getName(): string;

    /**
     * @return float
     */
    abstract public function getSalary(): float;
}

class Manager extends Employee
{
    protected $subordinates = [];

    /**
     * @param
     */
    public function addSubordinates(Employee $employee): void
    {
        $this->subordinates[] = $employee;
    }

    /**
     * @param
     */
    public function removeSubordinate(Employee $employee): void
    {
        $key = array_search($employee, $this->subordinates, true);
        if ($key !== false) {
            unset($this->subordinates[$key]);
        }
    }

    /**
     * @param
     */
    public function getSubordinates(Employee $employee)
    {
        return $this->subordinates;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getSalary(): float
    {
        return $this->salary;
    }
}

class RegularEmployee extends Employee
{
    protected $subordinates = [];

    public function getName(): string
    {
        return $this->name;
    }

    public function getSalary(): float
    {
        return $this->salary;
    }
}

$emp1 = new RegularEmployee('Joe', 700);
$emp2 = new RegularEmployee('John', 800);
$emp3 = new RegularEmployee('Jane', 850);

$man1 = new Manager('Man1', 900);
$man2 = new Manager('Man2', 950);

$man1->addSubordinates($emp1);
$man2->addSubordinates($emp2);
$man2->addSubordinates($emp3);

$employees = [$man1, $man2];
foreach ($employees as $e) {
    echo $e->getName() . ':' . $e->getSalary() . '<br/>';

    if ($e instanceof Manager) {
        foreach ($e->getSubordinates($e) as $subordinate) {
            echo '--' . $subordinate->getName() . ':' . $subordinate->getSalary() . '<br/>';
        }
    }
}