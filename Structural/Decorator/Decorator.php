<?php

namespace Structural\Decorator;

/**
 * Here is a simplified process of additional expenditures to existing
 * 'booking' product with new installation services for extra price
 */

interface Booking
{
    public function getName();

    public function getPrice();
}

class BasicBooking implements Booking
{
    protected string $name;
    protected float $price;

    /**
     * BasicBooking constructor.
     * @param string $name
     * @param float $price
     */
    public function __construct(string $name, float $price)
    {
        $this->name = $name;
        $this->price = $price;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return float
     */
    public function getPrice(): float
    {
        return $this->price;
    }
}

abstract class BookingDecorator implements Booking
{
    protected Booking $booking;

    /**
     * BookingDecorator constructor.
     * @param Booking $booking
     */
    public function __construct(Booking $booking)
    {
        $this->booking = $booking;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->booking->getName();
    }

    /**
     * @return float
     */
    public function getPrice(): float
    {
        return $this->booking->getPrice();
    }

}

class ServiceInstallationDecorator extends BookingDecorator
{
    protected $price;

    public function __construct(Booking $booking, $price)
    {
        parent::__construct($booking);
        $this->price = $price;
    }

    public function getName(): string
    {
        return $this->booking->getName() . '(with additional installation services) ';
    }

    public function getPrice(): float
    {
        return $this->booking->getPrice() + $this->price;
    }
}

class ExpeditedShipperDecorator extends BookingDecorator
{
    protected $price;

    public function __construct(Booking $booking, $price)
    {
        parent::__construct($booking);
        $this->price = $price;
    }

    public function getName(): string
    {
        return $this->booking->getName() . '(with shipping) ';
    }

    public function getPrice(): float
    {
        return $this->booking->getPrice() + $this->price;
    }
}

$booking = new BasicBooking('booking1234', 700);
$wrappedBooking = new ServiceInstallationDecorator($booking, 50);
$bookingWithShippingWithServices= new ExpeditedShipperDecorator($wrappedBooking, 10);

echo $bookingWithShippingWithServices->getName();
echo $bookingWithShippingWithServices->getPrice();